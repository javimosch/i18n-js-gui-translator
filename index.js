require('dotenv').config({
    silent: true
})
const app = require('express')()
app.use(require('express').static(process.cwd() + '/public'))
const funql = require('funql-api')
const jwt = require('jsonwebtoken');
const jwtSecret = process.env.JWT_SECRET || 'secret'
const sander = require('sander')
const { importFromString } = require('module-from-string')
const path = require('path');
const { match } = require('assert');

const helpers = {
    async getFilesFromProject(projectName) {
        let langs = (process.env.I18N_JS_GUI_TRANS_LANGS || 'fr,en').split(',')
        let pr = process.env.I18N_JS_GUI_TRANS_PROJECTS.split(',').find(pr => pr.split(':')[0].toLowerCase() == projectName.toLowerCase())
        prPath = pr.split(':')[1]
        let files = await sander.readdir(prPath)
        files = files.filter(file => langs.find(lang => file.indexOf(`.${lang}`) !== -1)).map(file => {
            return {
                file,
                lang: langs.find(lang => file.indexOf(`.${lang}`) !== -1)
            }
        })
        return files
    },
    setValueAtPath(obj,path,value){
        if(path.split('.').length===1){
            obj[path]= value
        }else{
            let pathArr = path.split('.')
            pathArr.splice(0,1)
            obj[path.split('.')[0]] = obj[path.split('.')[0]] || {}
            this.setValueAtPath(obj[path.split('.')[0]],pathArr.join('.'),value)
        }
    }
}

funql.middleware(app, {
    attachToExpress: true,
    api: {
        async saveTranslation(projectName, key, lang, value) {
            let files = await helpers.getFilesFromProject(projectName)
            let matchedFile = files.find(f => f.lang == lang)
            if (!matchedFile) {
                return false
            }
            try {
                let contents = (await sander.readFile(path.join(prPath, matchedFile.file))).toString('utf-8')
                let imported = await importFromString({
                    code: contents
                })       
                helpers.setValueAtPath(imported.default,key,value)
                let json = JSON.stringify(imported.default,null,4)
                contents = contents.split('export default ')[0]+'export default '+json
                await sander.writeFile(path.join(prPath, matchedFile.file), contents)
            } catch (err) {
                console.log(err)
                return false
            }
            return true
        },
        async getTranslations(projectName) {
            let files = await helpers.getFilesFromProject(projectName)
            let translations = [];
            await Promise.all(files.map(fileObj => {
                return (async () => {

                    let contents = (await sander.readFile(path.join(prPath, fileObj.file))).toString('utf-8')
                    try {
                        let imported = await importFromString({
                            code: contents
                        })

                        let cursor = imported.default

                        function collectKeys(cursor, path) {
                            Object.keys(cursor).forEach(key => {
                                if (typeof cursor[key] === 'object') {
                                    collectKeys(cursor[key], (path ? path + '.' : "") + key)
                                } else {
                                    let match = translations.find(t => t.key == path)
                                    if (match) {
                                        match[fileObj.lang] = cursor[key]
                                    } else {
                                        let newTranslation = {
                                            key: (path?path+'.':path)+key,
                                            [fileObj.lang]: cursor[key]
                                        }
                                        files.forEach(fo => {
                                            newTranslation[fo.lang] = newTranslation[fo.lang] || ""
                                        })
                                        translations.push(newTranslation)
                                    }
                                }
                            })
                        }
                        collectKeys(cursor, "")

                    } catch (err) {
                        console.log('Fail to import translation file', err)
                    }
                })();
            }))
            return translations
        },
        getUsers() {
            return (process.env.I18N_JS_GUI_TRANS_USERS && process.env.I18N_JS_GUI_TRANS_USERS.split(',').map(usr => usr.split(':')[0])) || []
        },
        checkToken(token) {
            return new Promise((resolve, reject) => {
                jwt.verify(token, jwtSecret, (err, data) => {
                    if (err) {
                        console.log(err)
                        resolve(false)
                    } else {
                        resolve(true)
                    }
                })
            })
        },
        async getLoggedUserRights(token) {
            let tokenPayload = await (() => {
                return new Promise((resolve, reject) => {
                    jwt.verify(token, jwtSecret, (err, data) => {
                        if (err) {
                            reject(err)
                        } else {
                            resolve(data)
                        }
                    })
                })
            })();
            let rights = (process.env.I18N_JS_GUI_TRANS_USERS_RIGHTS || "").split(',').find(usr => usr.split(':')[0].toLowerCase() == tokenPayload.data.login.toLowerCase()) || ""
            rights = rights.length > 0 ? rights.split(':')[1] : rights
            return rights.length > 0 ? rights.split('|') : (rights || [])
        },
        login(form) {
            return new Promise((resolve, reject) => {
                if (!(process.env.I18N_JS_GUI_TRANS_USERS && process.env.I18N_JS_GUI_TRANS_USERS.split(',').find(usr => usr.split(':')[0].toLowerCase() === form.login.toLowerCase() && form.pwd == usr.split(':')[1]))) {
                    resolve(false)
                }

                jwt.sign({
                    data: {
                        login: form.login
                    },
                    // exp: Math.floor(Date.now() / 1000) + (60 * 60 * process.env.SESSION_HOURS || 1)
                }, jwtSecret, {
                    expiresIn: '2h'
                }, function (err, token) {
                    if (err) {
                        reject(err)
                    } else {
                        resolve({
                            token
                        })
                    }
                });
            })
        }
    }
})
app.listen(process.env.PORT || 3000, () => console.log('ok listening', process.env.PORT || 3000))