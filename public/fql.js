import funql from 'https://cdn.jsdelivr.net/npm/funql-api@1.2.11/client.js'
import { Vue } from '/vue.js'
export const fql = funql(window.location.origin, {
    debug: (prefix = "") => {
        return function () {
            let args = Array.prototype.slice.call(arguments);
            args.unshift(prefix)
            console.log.apply(this, args)
        }
    }
})

Vue.use({
    install() {
        Vue.prototype.$fql = fql

    }
})
